import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-run',
  templateUrl: './run.component.html',
  styleUrls: ['./run.component.scss']
})
export class RunComponent implements OnInit {

  @Input() startTime: string; // decorate the property with @Input()

  @Input() endTime: string; // decorate the property with @Input()

  @Input() rangeStartTime: string; // decorate the property with @Input()

  @Input() rangeEndTime: string; // decorate the property with @Input()

  @Input() run: string; // decorate the property with @Input()




  constructor() { }

  initialPx: number;
  endPx: number;
  leftInPx:number;
  widthInPx:number;

  ngOnInit(): void {

    console.log("sssstart", this.startTime);
    console.log("eeeeend", this.endTime);
    console.log("raaaangeStart", this.rangeStartTime);

    this.getPixelTime(this.startTime, this.endTime , this.rangeStartTime);
  }

  getPixelTime(startTime, endTime , rangeStartTime){
    var startTime:any = new Date("01/01/2007 " + startTime);
    var endTime:any  = new Date("01/01/2007 " + endTime);
    var rangeStartTime:any  = new Date("01/01/2007 " + rangeStartTime);
    var startTimeDifference = this.getMinutesDifference(startTime, rangeStartTime);
    console.log(startTimeDifference ,  "startTimeeeee");
    var rangeTimeDifference = this.getMinutesDifference(endTime, startTime);

    console.log(rangeTimeDifference , "rangeeeeeeeee");

    this.initialPx = this.tranformTimeToMinutesPerPixels( moment(startTimeDifference).hours() , moment(startTimeDifference).minutes());
    this.widthInPx = this.tranformTimeToMinutesPerPixels( moment(rangeTimeDifference).hours() , moment(rangeTimeDifference).minutes());

    
   // (23/15)

  }

  getMinutesDifference (startTime, endTime){
    return new Date("01/01/2007 " + moment.utc(moment(startTime,"DD/MM/YYYY HH:mm:ss").diff(moment(endTime,"DD/MM/YYYY HH:mm:ss"))).format("H:mm"));
  }




  tranformTimeToMinutesPerPixels(hours, minutes){
    console.log(hours, "hours");
    console.log(minutes, "minutessssss");
   return ((hours*60) + minutes) * (23/15);
  }




}
