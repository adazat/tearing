import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline-grid',
  templateUrl: './timeline-grid.component.html',
  styleUrls: ['./timeline-grid.component.scss']
})
export class TimelineGridComponent implements OnInit {

  @Input() columns: any; // decorate the property with @Input()

  @Input() columnsArray: string; // decorate the property with @Input()

  @Input() columnWidth: string; // decorate the property with @Input()

  @Input() columnsTitles: string; // decorate the property with @Input()

  @Input() columnsSubTitles: string; // decorate the property with @Input()

  @Input() amColSpan: string; // decorate the property with @Input()

  @Input() pmColSpan: string; // decorate the property with @Input()

  @Input() columnsHourTitles: string; // decorate the property with @Input()

  @Input() runList: string; // decorate the property with @Input()

  @Input() item: string; // decorate the property with @Input()
  
  @Input() startTime: string; // decorate the property with @Input()

  rowSize: number;
  rowHeight: number = 35;
  columnHeight: number;

  constructor() { }

  ngOnInit(): void {
   this.rowSize = ((this.columns) * 23);
   this.columnHeight = this.runList.length * (this.rowHeight + 7);


    console.log(this.columns, "Heloooooo");
  }

}
