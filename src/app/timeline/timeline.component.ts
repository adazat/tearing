import {  Component, OnInit, Pipe,  } from '@angular/core';
import * as moment from 'moment';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  constructor() { }

  columns:number;
  hourColumns: number;
  columnsArray:number[];
  columnsTitles: string[] = new Array();
  columnsHourTitles: string[] = new Array(); 
  amColSpan:number = 0;
  pmColSpan:number = 0;
  columnsSubTitles: string[] = new Array();
  columnWidth:string;
  timeRange: number;
  runList: any[any] = [
    {
      id: 1234567888,
      startTime: "07:15",
      endTime: "09:11",
      startLocation: "A",
      dropOffLocation: "B",
      daysActive: {
        monday: true,
        tuesday: false, 
        wednesday: false,
        thursday: true, 
        friday: false,
        saturday: true, 
        sunday: false
      }
    },
    {
      id: 1234567888,
      startTime: "08:15",
      endTime: "10:14",
      startLocation: "A",
      dropOffLocation: "B",
      daysActive: {
        monday: true,
        tuesday: false, 
        wednesday: false,
        thursday: true, 
        friday: false,
        saturday: true, 
        sunday: false
      }
    },
    {
      id: 1234567888,
      startTime: "10:13",
      endTime: "13:17",
      startLocation: "A",
      dropOffLocation: "B",
      daysActive: {
        monday: true,
        tuesday: false, 
        wednesday: false,
        thursday: true, 
        friday: false,
        saturday: true, 
        sunday: false
      }
    },
    {
      id: 1234567888,
      startTime: "05:08",
      endTime: "11:44",
      startLocation: "A",
      dropOffLocation: "B",
      daysActive: {
        monday: true,
        tuesday: false, 
        wednesday: false,
        thursday: true, 
        friday: false,
        saturday: true, 
        sunday: false
      }
    },
    {
      id: 1234567888,
      startTime: "09:13",
      endTime: "14:10",
      startLocation: "A",
      dropOffLocation: "B",
      daysActive: {
        monday: true,
        tuesday: false, 
        wednesday: false,
        thursday: true, 
        friday: false,
        saturday: true, 
        sunday: false
      }
    },
    {
      id: 1234567888,
      startTime: "07:15",
      endTime: "09:11",
      startLocation: "A",
      dropOffLocation: "B",
      daysActive: {
        monday: true,
        tuesday: false, 
        wednesday: false,
        thursday: true, 
        friday: false,
        saturday: true, 
        sunday: false
      }
    }
]


  ngOnInit(): void {
    this.getTimeRange("4:00", "20:00");
    this.setColumnsWidth( this.timeRange, 1000, "quaters");
    this.setColumnsTitles(new Date("01/01/2007 4:00"), new Date("01/01/2007 20:00"), "quaters", this.columns);
    this.setColumnsWidth( this.timeRange, 1000, "");
    this.setHourColumnsTitles(new Date("01/01/2007 4:00"), new Date("01/01/2007 20:00"), "quaters", this.hourColumns);

  }


  getTimeRange (startHour, endHour){

    var timeStart = new Date("01/01/2007 " + startHour).getHours();
    var timeEnd = new Date("01/01/2007 " + endHour).getHours();

    this.timeRange = (timeEnd - timeStart);
  }

  setColumnsTitles(startTime, endTime, timeToDisplay, columnNumber){
    let currentTime = moment(startTime);

    for( let i= 0; i < columnNumber; i++ ){
      if(currentTime.hours() < 12){
        this.amColSpan++;
      }else{
        this.pmColSpan++;
      }

      console.log(this.columnsSubTitles, 'Subtitles');

      this.columnsTitles.push(currentTime.format("mm"));
      currentTime.add(15, 'm').toDate();
    }
  }

  setHourColumnsTitles(startTime, endTime, timeToDisplay, columnNumber){
    let currentTime = moment(startTime);

    this.setColumnsWidth( this.timeRange, 1000, "");

    for( let i= 0; i < columnNumber; i++ ){
      console.log(this.columnsSubTitles, 'Subtitles');

      this.columnsHourTitles.push(currentTime.format("hA"));
      currentTime.add(1, 'h').toDate();
    }
  }

  setColumnsWidth(timeRange, width, timeToDisplay){
    let columnWidth;

    switch (timeToDisplay) {
      case "quaters":
        columnWidth = ((width/timeRange)/2)/2
        this.columnWidth = columnWidth +" px";
        this.columns = width /columnWidth;
        this.columnsArray = new Array(width /columnWidth);

        break;
      case "half":
        columnWidth = (width/timeRange)/2
        this.columnWidth = columnWidth +" px";
        this.columns = width /columnWidth;
        this.columnsArray = new Array(width /columnWidth); 
        break;
      default:
        columnWidth = width / timeRange
        this.columnWidth = columnWidth +" px";
        this.hourColumns = width /columnWidth;
        this.columnsArray = new Array(width /columnWidth); 
        break;
    }

    console.log(timeRange, "Horas")
    console.log(width, "Ancho")
    console.log(timeToDisplay, "tiempo a mostrar ")

    console.log(this.columnWidth, "tiempo a mostrar ")
    console.log(this.columns, "tiempo a mostrar ")
    

  }
}
