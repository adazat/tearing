import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CheckBoxModule, InputsModule } from '@progress/kendo-angular-inputs';
import { DayPilotModule } from 'daypilot-pro-angular';
import { AppComponent } from './app.component';
import { DaypilotContainerComponent } from './daypilot-container/daypilot-container.component';
import { DaypilotPocFourComponent } from './daypilot-container/daypilot-poc-four/daypilot-poc-four.component';
import { DaypilotPocThreeComponent } from './daypilot-container/daypilot-poc-three/daypilot-poc-three.component';
import { RunComponent } from './timeline/timeline-grid/run/run.component';
import { TimelineGridComponent } from './timeline/timeline-grid/timeline-grid.component';
import { TimelineComponent } from './timeline/timeline.component';
import { TimesPipe } from './timeline/timesPipe/times-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TimelineComponent,
    TimelineGridComponent,
    TimesPipe,
    RunComponent,
    DaypilotContainerComponent,
    DaypilotPocThreeComponent,
    DaypilotPocFourComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DayPilotModule,
    InputsModule,
    CheckBoxModule,
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
