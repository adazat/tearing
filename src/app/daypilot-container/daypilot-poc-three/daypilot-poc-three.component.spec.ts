import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DaypilotPocThreeComponent } from './daypilot-poc-three.component';

describe('DaypilotPocThreeComponent', () => {
  let component: DaypilotPocThreeComponent;
  let fixture: ComponentFixture<DaypilotPocThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DaypilotPocThreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DaypilotPocThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
