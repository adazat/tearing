import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DayPilot, DayPilotSchedulerComponent } from 'daypilot-pro-angular';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-daypilot-poc-three',
  templateUrl: './daypilot-poc-three.component.html',
  styleUrls: ['./daypilot-poc-three.component.scss'],
})
export class DaypilotPocThreeComponent implements AfterViewInit {
  @ViewChild('scheduler')
  scheduler: DayPilotSchedulerComponent;

  events: any[] = [];

  config: any = {
    rowHeaderWidthAutoFit: false,
    eventStackingLineHeight: 20,
    autoScroll: 'Disabled',
    cellWidthSpec: 'Auto',
    treeEnabled: true,
    cellWidthMin: 40,
    timeHeaders: [
      {
        groupBy: 'Hour',
      },
      {
        groupBy: 'Cell',
        format: 'mm',
      },
    ],
    scale: 'CellDuration',
    cellDuration: 15,
    dynamicLoading: true,
    dynamicEventRendering: 'Disabled',
    useEventBoxes: 'Never',
    infiniteScrollingEnabled: false,
    rowMinHeight: 41,
    eventHeight: 41,
    rowClickHandling: 'Select',
    businessBeginsHour: 5,
    businessEndsHour: 19,
    showNonBusiness: false,
    businessWeekends: true,
    allowMultiSelect: true,
    allowMultiMove: true,
    eventClickHandling: 'Select',
    multiMoveVerticalMode: 'All',
    eventResizeHandling: 'Disabled',
    timeRangeSelectedHandling: 'Disabled',
    startDate: DayPilot.Date.today(),
    onTimeRangeSelected: (args) => {
      let dp = this.scheduler.control;
      DayPilot.Modal.prompt('Create a new event:', 'Event 1').then(function (
        modal
      ) {
        dp.clearSelection();
        if (!modal.result) {
          return;
        }
        dp.events.add(
          new DayPilot.Event({
            start: args.start,
            end: args.end,
            id: DayPilot.guid(),
            resource: args.resource,
            text: modal.result,
          })
        );
      });
    },
    eventMoveHandling: 'Update',
    onEventMoved: (args) => {
      this.scheduler.control.message('Event moved');
    },
    onEventResized: (args) => {
      this.scheduler.control.message('Event resized');
    },
    eventDeleteHandling: 'Update',
    onEventDeleted: (args) => {
      this.scheduler.control.message('Event deleted');
    },
    onBeforeEventRender: (args) => {
      if (args.data.phases) {
        // hide the default event content and tooltip
        args.data.barHidden = true;
        args.data.html = '';
        args.data.toolTip = '';

        if (!args.data.areas) {
          args.data.areas = [];
        }
        args.data.phases.forEach((phase) => {
          args.data.areas.push({
            start: phase.start,
            end: phase.end,
            top: 0,
            bottom: 0,
            html: phase.text,
          });
        });
      }
    },
    onBeforeRowHeaderRender: (args) => {
      console.log('this works?');
      var row = args.row;
      var selected = this.scheduler.control.rows.selection.isSelected(row);
      console.log('this works? row', row);
      if (row.children().length === 0) {
        row.areas = [
          {
            left: 0,
            top: 9,
            width: 13,
            height: 13,
            html: selected
              ? "<input type='checkbox' checked>"
              : "<input type='checkbox'>",
            onClick: function (args) {
              if (this.scheduler.control.rows.selection.isSelected(row)) {
                this.scheduler.control.rows.selection.remove(row);
              } else {
                this.scheduler.control.rows.selection.add(row);
              }
            },
          },
        ];
      }
    },
    onBeforeCellRender: function (args) {
      const r = this.rows.find(args.cell.resource);
      const businessBeginsHour = 5;
      const businessEndsHour = 19;
      const cellDuration = this.cellDuration === 15 ? 4 : 2;
      const variableWidth =
        (businessEndsHour - businessBeginsHour) * cellDuration;
      if (r.id === 'frozen1') {
        args.cell.properties.backColor = '#f3f3f3';
        args.cell.properties.areas = [
          {
            horizontalAlignment: 'bottom',
            padding: 10,
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            cssClass: 'cell',
            style: `display: flex;
            justify-content: flex-start;
            align-items: center;
            z-index:10;
            color: black;
            width: calc(100% * ${variableWidth} )`,
            text: 'Drop untiered runs to create a new route',
          },
          {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            style: 'display: none; opacity: 0.2; background-color: gray;',
            visibility: 'Hover',
          },
        ];
      } else if (r.id === 'frozen2') {
        args.cell.properties.backColor = '#f3f3f3';
        args.cell.properties.areas = [
          {
            horizontalAlignment: 'bottom',
            padding: 10,
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            cssClass: 'cell2',
            style: `display: flex;
            justify-content: flex-start;
            align-items: center;
            z-index:10;
            color: black;
            width: calc(100% * ${variableWidth} )`,
            text: 'Drop untiered runs to create a new route 2',
          },
          {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            style: 'display: none; opacity: 0.2; background-color: gray;',
            visibility: 'Hover',
          },
        ];
      }
    },
    onAfterRender: (args) => {
      const element = document.querySelectorAll('.cell');
      element.forEach((e, index) => {
        if (index === 0) {
          e.classList.add('flex-show');
        } else {
          e.classList.add('none');
        }
      });

      const element2 = document.querySelectorAll('.cell2');
      element2.forEach((e, index) => {
        if (index === 0) {
          e.classList.add('flex-show');
        } else {
          e.classList.add('none');
        }
      });
    },
    dragOutAllowed: true,
  };

  constructor(private ds: DataServiceService) {}

  ngAfterViewInit(): void {
    this.ds
      .getResources2()
      .subscribe((result) => (this.config.resources = result));

    const from = this.scheduler.control.visibleStart();
    const to = this.scheduler.control.visibleEnd();
    this.ds.getEvents3(from, to).subscribe((result) => {
      this.events = result;
    });
    this.ds.getResourcesHeaders().subscribe((result) => {
      this.config.rowHeaderColumns = result;
    });
    this.ds.getEvents3Links(from, to).subscribe((result) => {
      this.config.links = result;
    });
  }
}
