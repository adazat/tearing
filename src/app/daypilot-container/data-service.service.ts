import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DayPilot } from 'daypilot-pro-angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  today = DayPilot.Date.today();


  rowHeaderColumns0: any[] = [
    { text: 'Code', display: "name" },
    { text: 'Vehicle', display: "vehicle" },
  ];

  resources: any[] = [
      { name: '101-R-28-15', id: 'R1' , vehicle: 'L162'},
      { name: '101-R-28-16', id: 'R2', vehicle: 'L162' },
      { name: '101-R-28-17', id: 'R3', vehicle: 'L162', unavailable: true },
      { name: '101-R-28-18', id: 'R4', vehicle: 'L162'}
  ];


 rowHeaderColumns: any[] = [{
  title: 'ACTIONS', display: 'actions', hidden: false, customizable: false, width: 70
 },
    { text: 'Code', display: "name" },
    { text: 'Vehicle', display: "vehicle", width: 80 },
    { }
  ];


  resources2: any[] = [
      { name: '101-R-28-15', vehicle: 'L162', id:'101-R-28-15',  avl: "A215640", driver: "Austin Cambridge", aide: "Maria Jordan" },
      { name: '101-R-28-16', vehicle: 'L163', id: '101-R-28-16', avl: "Z000941", driver: "Matthew Johnson", aide: "Jasper Williams"},
      { name: '101-R-28-17', vehicle: 'L164', id:'101-R-28-17',  avl: "0100948", driver: "Matthew Brown", aide: "Jasper Williams"},
      { name: '101-R-28-18', vehicle: 'L165', id: '101-R-28-18', avl: "WR00987", driver: "Naomi Johnson", aide: "Melanie Jones"},
      { name: "", resource:'forzen', id: "frozen1", frozen: "top" },
      { name: "", resource:'forzen', id: "frozen2", frozen: "top" }
  ];

  events: any[] = [
    {
      id: '1',
      resource: 'R1',
      start: '2018-10-03',
      end: '2018-10-08',
      text: 'Scheduler Event 1',
      color: '#e69138'
    },
    {
      id: '2',
      resource: 'R2',
      start: '2018-10-02',
      end: '2018-10-05',
      text: 'Scheduler Event 2',
      color: '#6aa84f'
    },
    {
      id: '3',
      resource: 'R2',
      start: '2018-10-06',
      end: '2018-10-09',
      text: 'Scheduler Event 3',
      color: '#3c78d8'
    }
  ];


  events2: any[] = [
    {
      id: '1',
      resource: 'R1',
      start:'2020-12-22T09:15:00',
      end: '2020-12-22T10:00:00',
      text: 'Scheduler Event 1',
      color: '#e69138'
    },
    {
      id: '2',
      resource: 'R2',
      start: '2020-12-22T14:30:00',
      end: '2020-12-22T15:15:00',
      text: 'Scheduler Event 2',
      color: '#6aa84f'
    },
    {
      id: '3',
      resource: 'R3',
      start: '2020-12-22T11:30:00',
      end: '2020-12-22T12:15:00',
      text: 'Scheduler Event 3',
      color: '#3c78d8'
    }
  ];


  events3: any[] = [
    {
      id: '3001',
      resource: '101-R-28-15',
      start: this.today.addHours(5),
      end: this.today.addHours(12),
      text: 'Scheduler Event 1',
      color: '#e69138',
      cssClass: 'eventContainerClass',
      phases: [
        { start: this.today.addHours(5) , end: this.today.addHours(10), text: "Preparation", toolTip: "Preparation"},
        { start: this.today.addHours(10), end: this.today.addHours(11), text: "Main Phase", toolTip: "Main Phase"},
        { start: this.today.addHours(11), end: this.today.addHours(12), text: "Evaluation", toolTip: "Evaluation"}
      ],
      container: 2
    },
    {
      id: '3002',
      resource: '101-R-28-15',
      start: this.today.addHours(13),
      end: this.today.addHours(15),
      text: 'Scheduler Event 2',
      color: '#6aa84f',
      cssClass: 'eventContainerClass',
      phases: [
        { start: this.today.addHours(13), end: this.today.addHours(14), text: "Preparation", toolTip: "Preparation"},
        { start: this.today.addHours(14), end: this.today.addHours(14).addMinutes(30), text: "Main Phase", toolTip: "Main Phase"},
        { start: this.today.addHours(14).addMinutes(30), end: this.today.addHours(15), text: "Evaluation", toolTip: "Evaluation"}
      ]
    },
    {
      id: '3003',
      resource: '101-R-28-16',
      start: this.today.addHours(11).addMinutes(30),
      end: this.today.addHours(12).addMinutes(15),
      text: 'Scheduler Event 3',
      cssClass: 'schedule-hours',
      html: "<p>To: Some School <br/><span>M Tu W Th F Sa Su</span></p>",
      container: 1
    },
    {
      id: '30031',
      resource: '101-R-28-16',
      start: this.today.addHours(12).addMinutes(30),
      end: this.today.addHours(13).addMinutes(15),
      text: 'Scheduler Event 3',
      cssClass: 'schedule-hours',
      html: "<p>To: Some School <br/><span>M Tu W Th F Sa Su</span></p>",
      container: 1
    }
  ];

  events3Links = [
    {
      from: "3003",
      to: "30031"
    },
    {
      from: "3001",
      to: "3002"
    },
  ]

  constructor(private http: HttpClient) {
  }

  getEvents(from: DayPilot.Date, to: DayPilot.Date): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.events);
      }, 200);
    });

    // return this.http.get("/api/events?from=" + from.toString() + "&to=" + to.toString());
  }

  getEvents2(from: DayPilot.Date, to: DayPilot.Date): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.events2);
      }, 200);
    });

    // return this.http.get("/api/events?from=" + from.toString() + "&to=" + to.toString());
  }

  getEvents3(from: DayPilot.Date, to: DayPilot.Date): Observable<any[]> {
    console.log(this.today, "Today");
    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.events3);
      }, 200);
    });

    // return this.http.get("/api/events?from=" + from.toString() + "&to=" + to.toString());
  }

  getEvents3Links(from: DayPilot.Date, to: DayPilot.Date): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.events3Links);
      }, 200);
    });

    // return this.http.get("/api/events?from=" + from.toString() + "&to=" + to.toString());
  }

  getResources(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.resources);
      }, 200);
    });

    // return this.http.get("/api/resources");
  }


  getResources2(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.resources2);
      }, 200);
    });

    // return this.http.get("/api/resources");
  }

  getResourcesHeaders(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.rowHeaderColumns);
      }, 200);
    });

    // return this.http.get("/api/resources");
  }
}
