import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { DayPilot, DayPilotSchedulerComponent } from 'daypilot-pro-angular';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-daypilot-poc-four',
  templateUrl: './daypilot-poc-four.component.html',
  styleUrls: ['./daypilot-poc-four.component.scss']
})
export class DaypilotPocFourComponent implements AfterViewInit {

  @ViewChild('scheduler')
  scheduler: DayPilotSchedulerComponent;

  events: any[] = [];

  config: any = {
    cellWidthSpec: "Auto",
    treeEnabled: true,
    cellWidthMin: 20,
    timeHeaders: [
      {
        "groupBy": "Hour"
      },
      {
        "groupBy": "Cell",
        "format": "mm"
      }
    ],
    scale: "CellDuration",
    cellDuration: 15,
    startDate: DayPilot.Date.today(),
    businessBeginsHour: 9,
    businessEndsHour: 18,
    showNonBusiness: false,
    useEventBoxes: "Never",
    allowMultiSelect: true,
    allowMultiMove: true,
    eventMoveHandling: 'Update',
    dragOutAllowed: true,
    rectangleSelectHandling: "EventSelect",
    rectangleSelectMode: "Free",
    eventClickHandling: "Select",
    multiMoveVerticalMode: "All",
    onTimeRangeSelected: args => {
      let dp = this.scheduler.control;
      DayPilot.Modal.prompt("Create a new event:", "To: School George <br/> M Tu W Th F Sa Su").then(function(modal) {
        dp.clearSelection();
        if (!modal.result) { return; }
        dp.events.add(new DayPilot.Event({
          start: args.start,
          end: args.end,
          id: DayPilot.guid(),
          resource: args.resource,
          text: modal.result
        }));
      });
    },
    onEventMoved: args => {
      this.scheduler.control.message("Event moved");
    },
    eventResizeHandling: "Update",
    onEventResized: args => {
      this.scheduler.control.message("Event resized");
    },
    eventDeleteHandling: "Update",
    onEventDeleted: args => {
      this.scheduler.control.message("Event deleted");
    },
    rowMoveHandling: "Update",
    onRowMove : function (args) {
      this.message("Event deleted: " + args.e.text());
    },
    onRowMoving : function (args) {
      this.message("Event deleted: " + args.e.text());
    },
    onBeforeRowHeaderRender : args => {
      var dp = this.scheduler.control;
      var row = args.row;
      var selected = dp.rows.selection.isSelected(row);
      var column = args.row.columns.length > 0 ? args.row.columns[0] : args.row;
      if (row.children().length === 0) {
        column.areas = [
          {
            left: 0,
            top: 9,
            width: 13,
            height: 13,
            html: selected ?
            "<div class='input-box'>" +
              "<input type='checkbox' id='activeSunday' class='k-checkbox' checked='true' kendoCheckBox />" +
              "<label class='k-checkbox-label' for='activeSunday'></label>" +
            "</div>"
            : "<div class='input-box'>" +
                "<input type='checkbox' id='activeSunday' class='k-checkbox' kendoCheckBox />" +
                "<label class='k-checkbox-label' for='activeSunday'></label>" +
              "</div> ",
            onClick: function (args) {
              if (dp.rows.selection.isSelected(row)) {
                dp.rows.selection.remove(row);
              }
              else {
                dp.rows.selection.add(row);
              }
            }
          }
        ];
      };
      //LINK ON TEXT
      args.row.columns[1].areas = [
        {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          cssClass: "column-link",
          html: args.row.data.name,
        }
      ];
    },
    rowClickHandling: "Select",
    onRowSelected: function(args) {
      var msg = "This row was " + (args.selected ? "" : "de") + "selected: " + args.row.name;
      this.message(msg);
    },
    onBeforeEventRender: args => {
      if (args.data.phases) {

        // hide the default event content and tooltip
        args.data.barHidden = true;
        args.data.html = '';
        args.data.toolTip = '';
        args.cssClass = '';

        if (!args.data.areas) {
          args.data.areas = [];
        }
        args.data.phases.forEach(phase => {
          args.data.areas.push({
            start: phase.start,
            end: phase.end,
            top: 0,
            bottom: 0,
            css: phase.css,
            style: "overflow:hidden; padding: 3px; box-sizing: border-box;",
            background: phase.background,
            html: phase.text,
            toolTip: phase.toolTip,
            cssClass: 'eventClass',
            });
        });
      }
    },
    onBeforeRowHeaderColumnRender: function (args) {
      if (args.column.data.display === 'name') {
        args.column.html = " &nbsp; &nbsp; &nbsp;" + args.column.html;
        args.column.areas = [
          {
            left: 0,
            top: 5,
            width: 13,
            height: 13,
            visibility: "Visible",
            html: "<div class='input-box'>" +
              "<input type='checkbox' id='activeSunday' class='k-checkbox' kendoCheckBox />" +
              "<label class='k-checkbox-label' for='activeSunday'></label>" +
            "</div>"
          }
        ];
      }
      if (args.column.data.display === 'vehicle') {
        args.column.html += ' <span class="k-icon k-i-sort-desc-sm"></span>';
      }
      if (args.column.data.display === 'name') {
        args.column.html += ' <span class="k-icon k-i-sort-asc-sm"></span>';
      }
    },
    onBeforeCornerRender: function (args) {
      const dp = args.control;
      args.areas = [
        {
          action: "ContextMenu",
          icon: "k-icon k-i-more-vertical",
          cssClass: "area-open-menu",
          menu: new DayPilot.Menu({
            onShow: function (args) {
              var menu = this;
              menu.items = [];
              dp.rowHeaderColumns.forEach(function (col) {
                if (col.customizable === true) {
                  menu.items.push({
                    _column: col,
                    text: col.text,
                    icon: col.hidden ? "" : "k-icon k-i-check",
                    onClick: function (args) {
                      // hide the menu, normally it stays visible until onClick completes
                      menu.hide();
                      var column = args.item._column;
                      column.hidden = !column.hidden;
                      // optimized update
                      dp.update({ rowHeaderColumns: dp.rowHeaderColumns });
                    }
                  });
                }
              });
              var visible = menu.items.filter(function (item) { return !item._column.hidden; });
              // only one column visible, prevent action
              if (visible.length === 1) {
                visible[0].disabled = true;
              }
            }
          })
        }
      ]
    }
  };
  rowHeaderColumns: any[] = [
    { text: 'Code', display: "name", customizable: false, hidden: false },
    { text: 'Vehicle', display: "vehicle", customizable: true, hidden: false, width: 100 },
    { text: 'AVL', display: "avl", customizable: true, hidden: true },
    { text: 'Driver', display: "driver", customizable: true, hidden: true },
    { text: 'Aide', display: "aide", customizable: true, hidden: true }
  ];

  constructor(private ds: DataServiceService) {
  }

  ngAfterViewInit(): void {
    this.ds.getResources2().subscribe(result => this.config.resources = result);
    const from = this.scheduler.control.visibleStart();
    const to = this.scheduler.control.visibleEnd();
    this.ds.getEvents3(from, to).subscribe(result => {
      this.events = result;
    });

    this.ds.getEvents3Links(from, to).subscribe(result => {
      this.config.links  = result;
    });
    this.config.rowHeaderColumns  = this.rowHeaderColumns;
  }

}
