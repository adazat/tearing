import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DaypilotPocFourComponent } from './daypilot-poc-four.component';

describe('DaypilotPocFourComponent', () => {
  let component: DaypilotPocFourComponent;
  let fixture: ComponentFixture<DaypilotPocFourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DaypilotPocFourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DaypilotPocFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
