import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DaypilotContainerComponent } from './daypilot-container.component';

describe('DaypilotContainerComponent', () => {
  let component: DaypilotContainerComponent;
  let fixture: ComponentFixture<DaypilotContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DaypilotContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DaypilotContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
