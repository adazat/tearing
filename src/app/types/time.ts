export class Time {
    startTime: string;
    endTime: string;
}

export class TimeToDisplay {
    quarters : number = 15;
    half: number = 30;
}