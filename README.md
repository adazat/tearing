
## Install and fresh start

https://code.daypilot.org/72120/angular-10-scheduler-quick-start-project


## Important Elements of DayPilot 

- Events: 

![](images/event.png)

- Resources: 

![](images/resources.png)

- Configurations:

```sh
config: any = {
    cellWidthSpec: "Auto",
    treeEnabled: true,
    cellWidthMin: 15,
    timeHeaders: [
      {
        "groupBy": "Hour"
      },
      {
        "groupBy": "Cell",
        "format": "mm"
      }
    ],
    scale: "CellDuration",
    cellDuration: 15,
    startDate: DayPilot.Date.today()
	}
```

For more information about this elements:
Events: https://doc.daypilot.org/scheduler/events/
Resources: https://doc.daypilot.org/scheduler/resource-loading/
Config: https://api.daypilot.org/daypilot-scheduler-properties/

## Config 

After you create and customize your config, add it to the component of daypilot-scheduler

```sh
<daypilot-scheduler [config]="config" [events]="events" #scheduler></daypilot-scheduler>
```


## Time Range or Grid Range 

To customize the time and grid range you have to use the properties: 
	
```sh
scale: "CellDuration"
```

Defines the time range that would be display for this case we use: "CellDuration" because daypilot does not have as default 15 minutes to learn more about the scale you can check here: https://api.daypilot.org/daypilot-scheduler-scale/

```sh
 timeHeaders: [
      {
        "groupBy": "Hour"
      },
      {
        "groupBy": "Cell",
        "format": "mm"
      }
    ]
```

timeHeaders Is used to define the header and the subHeader of the timeline: 



```sh
 cellDuration: 15
```

cellDuration This defines how the hours are going to be split in this case 15 minutes 


```sh
  startDate: DayPilot.Date.today()
	}
```

startDate This defines the timeline range for our case will be always the current day


## Events 


```sh
  events: any[] = [
    {
      id: '1',
      resource: 'R1',
      start: '2018-10-03',
      end: '2018-10-08',
      text: 'Scheduler Event 1',
      color: '#e69138',
      cssClass: 'eventContainerClass'
    }
	]
```

Here you can find the structure of the events is important that the start and the end properties are in the current date. 

To divide an Event in different phases you have to use the property phases: 

```sh

	{
      id: '3001',
      resource: '101-R-28-15',
      start: '2020-12-22T09:05:00',
      end: '2020-12-22T12:00:00',
      text: 'Scheduler Event 1',
      color: '#e69138',
      cssClass: 'eventContainerClass',
      phases: [
        { start: "2020-12-22T09:05:00", end: "2020-12-22T10:00:00", text: "Preparation", toolTip: "Preparation"},
        { start: "2020-12-22T10:00:00", end: "2020-12-22T11:15:00", text: "Main Phase", toolTip: "Main Phase"},
        { start: "2020-12-22T11:15:00", end: "2020-12-22T12:00:00", text: "Evaluation", toolTip: "Evaluation"}
      ],
      container: 2
    }

```

And to add to event with a line you have to use the property container for the two events that you want to link:


```sh
	{
      id: '30031',
      resource: '101-R-28-16',
      start: '2020-12-22T12:30:00',
      end: '2020-12-22T13:15:00',
      text: 'Scheduler Event 3',
      html: "<p>To: Some School </p> <p> M Tu W Th F Sa Su</p>",
      container: 1
    },
	{
      id: '3003',
      resource: '101-R-28-16',
      start: '2020-12-22T11:30:00',
      end: '2020-12-22T12:15:00',
      text: 'Scheduler Event 3',
      html: "<p>To: Some School </p> <p> M Tu W Th F Sa Su</p>",
      container: 1
    }
```

Make sure that they are in the same resource and use the following property of the configuration to create the link between the events.

```sh
  config: any = {
		links : [
    {
      from: "3003",
      to: "30031"
    },
    {
      from: "3001",
      to: "3002"
    },
  ]
}
```

Remember to use the id to link them. 


After you create and customize your events, add them to the component of daypilot-scheduler

```sh
<daypilot-scheduler [config]="config" [events]="events" #scheduler></daypilot-scheduler>
```

## Style

DayPilot uses a common css class for all their elements you can find more info here:
https://doc.daypilot.org/scheduler/css-classes/

This css change are only recognize by the parent Styles.scss in this case

To have more control about how to customize your events and phases you should use the property in the event cssClass object:

```sh
  events: any[] = [
    {
      id: '1',
      resource: 'R1',
      start: '2018-10-03',
      end: '2018-10-08',
      text: 'Scheduler Event 1',
      cssClass: 'eventContainerClass'
    }
	]
```

## Consideration for Compass Tearing 

1. The resources and the events are independent that means that if you move one event the resource will remain in his original position. Suggestion: Create a observable for both resources and event use the event handler when the event is moved:  https://doc.daypilot.org/scheduler/event-moving/

2. When you move and event from runs to tearing you have transform or execute the process of tearing when the event is dropped to do that you can use the eventDrop handler: https://doc.daypilot.org/scheduler/external-drag-and-drop/

3. The Html inside of each event can be change but its container no, for that reason it will be difficult to display information on the event itself. 


## Possible improvements 

1. Reduce the time-range instead of the whole day used an 8-hour range.
2. Increase the event height to use better its content and display more info. 
3. Block the horizontal movement.
4. Add the Column headers picker. https://code.daypilot.org/49959/javascript-scheduler-show-hide-columns-using-context-menu





